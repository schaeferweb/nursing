<?php

namespace Joekolade\Nursing\Controller;

/***
 * This file is part of the "Pflegeberufe" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2017 Joe Schäfer <mail@schaefer-webentwicklung.de>, Schäfer – Büro für Webentwicklung
 ***/

use Joekolade\Nursing\Domain\Model\Employer;
use Joekolade\Nursing\Domain\Model\Location;
use Joekolade\Nursing\Domain\Model\Region;

/**
 * BackendController
 */
class BackendController extends \Joekolade\Nursing\Controller\AbstractController
{

    /**
     * employerRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\EmployerRepository
     * @inject
     */
    protected $employerRepository = null;

    /**
     * @var array
     */
    protected $csv;

    /**
     * @var array
     */
    protected $regionMapping = [
        'BW' => 'Baden-Württemberg',
        'BY' => 'Bayern',
    ];

    /**
     * @var array
     */
    protected $areaMapping = [
        'Alb-Donau-Kreis' => 'Alb-Donau-Kreis',
        'Biberach' => 'Kreis Biberach',
        'Bodenseekreis' => 'Bodenseekreis',
        'Calw' => 'Kreis Calw',
        'Dillingen a.d. Donau' => 'Kreis Dillingen a.d. Donau',
        'Esslingen' => 'Kreis Esslingen',
        'Freudenstadt' => 'Kreis Freudenstadt',
        'Göppingen' => 'Kreis Göppingen',
        'Günzburg' => 'Kreis Günzburg',
        'Heidenheim' => 'Kreis Heidenheim',
        'Kaufbeuren, Stadt' => 'Kreis Kaufbeuren',
        'Kempten (Allgäu), Stadt' => 'Kreis Kempten (Allgäu)',
        'Konstanz' => 'Kreis Konstanz',
        'Lindau (Bodensee)' => 'Kreis Lindau (Bodensee)',
        'Memmingen, Stadt' => 'Kreis Memmingen',
        'Neu-Ulm' => 'Kreis Neu-Ulm',
        'Oberallgäu' => 'Oberallgäu',
        'Ostalbkreis' => 'Ostalbkreis',
        'Ostallgäu' => 'Ostallgäu',
        'Ravensburg' => 'Kreis Ravensburg',
        'Rems-Murr-Kreis' => 'Rems-Murr-Kreis',
        'Reutlingen' => 'Kreis Reutlingen',
        'Rottweil' => 'Kreis Rottweil',
        'Schwarzwald-Baar-Kreis' => 'Schwarzwald-Baar-Kreis',
        'Sigmaringen' => 'Kreis Sigmaringen',
        'Tübingen' => 'Kreis Tübingen',
        'Tuttlingen' => 'Kreis Tuttlingen',
        'Ulm, Universitätsstadt' => 'Kreis Ulm',
        'Unterallgäu' => 'Unterallgäu',
        'Zollernalbkreis' => 'Zollernalbkreis',
    ];

    /**
     * @var array
     */
    protected $objectCount = [
        'regions' => 0,
        'locations' => 0,
        'employers' => 0,
    ];

    protected $candidates = [
        'regions' => [],
        'locations' => [],
        'employers' => [],
    ];

    public function listAction()
    {
    }

    public function importAction()
    {
        /** @var array $arguments */
        $arguments = $this->request->getArguments();

        if ($this->request->hasArgument('csv') && $this->request->getArgument('csv')['error'] === 0) {

            $this->csv = $this->csvToArray($this->request->getArgument('csv')['tmp_name']);
        }

        $this->buildCandidates();

        $this->buildRegions();

        $this->buildLocations();

        $this->buildEmployers();

        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump(
            $this->objectCount,
            'Debug: ' . __FILE__ . ' in Line: ' . __LINE__
        );

        die();

        $this->view->assign('arguments', $arguments);
    }

    public function refineTitlesAction()
    {
        // Regions
        $regions = $this->regionRepository->findAll();
        foreach ($regions as $region) {
            $this->fixTitle($region, $this->regionMapping, $this->regionRepository);
            $this->fixTitle($region, $this->areaMapping, $this->regionRepository);
        }

        $this->persistenceManager->persistAll();

        $this->view->assign('regions', $regions);
    }

    public function fetchCoordsAction()
    {
        $count = 0;
        $updated = [];
        $employers = $this->employerRepository->findAll();

        /** @var \Joekolade\Nursing\Domain\Model\Employer $employer */
        foreach ($employers as $employer) {
            // Has no coordinates
            if ($count < $this->settings['coordfetchmaximum'] && !$employer->getGeolocation() && $employer->getGeoaddress()) {
                $loc = $this->getCoordinates($employer->getGeoaddress());
                if ($loc) {
                    $employer->setGeolocation($loc);
                    $count++;
                    $this->employerRepository->update($employer);
                    $updated[] = $employer;
                }
            }
        }

        $havelocations = $this->employerRepository->countByGeolocation('');

        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump(
        	$havelocations,
        	'Debug: ' . __FILE__ . ' in Line: ' . __LINE__
        );

        $this->view->assign('employers', $updated);
        $this->view->assign(
            'noloocations',
            $havelocations - intval($this->settings['coordfetchmaximum'])
        );
    }

    protected function fixTitle(&$object, $mapping, $repository)
    {
        if (count($mapping[$object->getTitle()])) {
            $object->setTitle($mapping[$object->getTitle()]);
            $repository->update($object);
        }
    }

    function csvToArray($filename = '', $delimiter = ';')
    {

        ini_set('auto_detect_line_endings', true);
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    $header = array_map('trim', $row);
                } else {
                    if (count($header) > count($row)) {
                        $difference = count($header) - count($row);
                        for ($i = 1; $i <= $difference; $i++) {
                            $row[count($row) + 1] = $delimiter;
                        }
                    }
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
        return $data;
    }

    protected function buildCandidates()
    {
        $helperRegions = [];
        $helperLocations = [];
        foreach ($this->csv as $row) {
            if (!in_array($row['Bundesland'], $helperRegions)) {
                $helperRegions[] = $row['Bundesland'];
                $this->candidates['regions'][] = ['title' => $row['Bundesland']];
            }
            if (!in_array($row['Kreis'], $helperRegions)) {
                $helperRegions[] = $row['Kreis'];
                $this->candidates['regions'][] = ['title' => $row['Kreis']];
            }
            if (!in_array($row['Stadt'], $helperLocations)) {
                $helperLocations[] = $row['Stadt'];
                $this->candidates['locations'][] = [
                    'title' => $row['Stadt'],
                    'zip' => $row['PLZ'],
                    'regions' => [
                        $row['Kreis'],
                        $row['Bundesland']
                    ],
                ];
            }
            $this->candidates['employers'][] = [
                'title' => $row['Firmenname'],
                'address' => '<p>' . $row['Strasse'] . '<br>' . $row['PLZ'] . ' ' . $row['Stadt'] . '</p>',
                'geoaddress' => $row['Strasse'] . ', ' . $row['PLZ'] . ' ' . $row['Stadt'],
                'telephone' => $row['Tel_Vorwahl'] . ' / ' . $row['Tel'],
                'website' => $row['Website'],
                'email' => $row['EMail'],
                'location' => $row['Stadt']
            ];
        }
    }

    protected function buildRegions()
    {

        foreach ($this->candidates['regions'] as $regionCandidate) {
            /** @var Region $region */
            $region = new Region();

            $this->setFields($regionCandidate, $region);

            $this->regionRepository->add($region);

            $this->objectCount['regions']++;
        }

        $this->persistenceManager->persistAll();
    }

    protected function buildLocations()
    {
        foreach ($this->candidates['locations'] as $locationCandidate) {
            /** @var Location $location */
            $location = new Location();

            $this->setFields($locationCandidate, $location);

            $this->locationRepository->add($location);

            $this->objectCount['locations']++;
        }

        $this->persistenceManager->persistAll();
    }

    protected function buildEmployers()
    {
        foreach ($this->candidates['employers'] as $employerCandidate) {
            /** @var Employer $employer */
            $employer = new Employer();

            $this->setFields($employerCandidate, $employer);

            $this->employerRepository->add($employer);

            $this->objectCount['employers']++;
        }

        $this->persistenceManager->persistAll();
    }

    /**
     * @param array $dataArray
     * @param mixed &$object
     */
    protected function setFields($dataArray, &$object)
    {
        foreach ($dataArray as $key => $item) {
            switch ($key) {
                case 'title':
                    $object->setTitle($item);
                    break;
                case 'zip':
                    $object->setZip($item);
                    break;
                case 'address':
                    $object->setAddress($item);
                    break;
                case 'geoaddress':
                    $object->setGeoaddress($item);
                    break;
                case 'telephone':
                    $object->setTelephone($item);
                    break;
                case 'website':
                    $object->setWebsite($item);
                    break;
                case 'email':
                    $object->setEmail($item);
                    break;
                case 'regions':
                    /** @var Region $object */
                    foreach ($item as $region) {
                        $reg = $this->regionRepository->findOneByTitle($region);
                        if (!empty($reg)) {
                            $object->addRegion($reg);
                        }
                    }
                    break;
                case 'location':
                    /** @var Employer $object */
                    $loc = $this->locationRepository->findOneByTitle($item);
                    if (!empty($loc)) {
                        $object->setLocation($loc);
                    }
                    break;
                default:
                    return false;
            }
        }
    }

    /**
     * geocoord function from
     * https://colinyeoh.wordpress.com/2013/02/12/simple-php-function-to-get-coordinates-from-address-through-google-services/
     *
     * @param string $address
     * @return string
     */
    function getCoordinates($address)
    {

        $address = str_replace(" ", "+",
            $address); // replace all the white space with "+" sign to match with google search pattern

        $url = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";

        $response = file_get_contents($url);

        $json = json_decode($response, true); //generate array object from the response from the web

        if (count($json['results'])) {
            return ($json['results'][0]['geometry']['location']['lat'] . ", " . $json['results'][0]['geometry']['location']['lng']);
        }
        return false;
    }
}
