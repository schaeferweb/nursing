<?php
namespace Joekolade\Nursing\Controller;

/***
 *
 * This file is part of the "Pflegeberufe" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Joe Schäfer <mail@schaefer-webentwicklung.de>, Schäfer – Büro für Webentwicklung
 *
 ***/

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * AbstractController
 */
class AbstractController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * Handling for filter options
     *
     * @var string
     */
    protected $OBJECT_SWITCH = '';

    /**
     * positionRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\PositionRepository
     * @inject
     */
    protected $positionRepository = null;

    /**
     * typeRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\TypeRepository
     * @inject
     */
    protected $typeRepository = null;

    /**
     * locationRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\LocationRepository
     * @inject
     */
    protected $locationRepository = null;

    /**
     * regionRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\RegionRepository
     * @inject
     */
    protected $regionRepository = null;

    /**
     * employmentRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\EmploymentRepository
     * @inject
     */
    protected $employmentRepository = null;

    /**
     * extraRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\ExtraRepository
     * @inject
     */
    protected $extraRepository = null;

    /**
     * cacheUtility
     *
     * @var \TYPO3\CMS\Core\Cache\CacheManager
     */
    protected $cacheInstance;

    /**
     * persistence manager
     *
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;

    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected $cObj;

    public function initializeAction() {
        $this->cacheInstance = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')->getCache("nursing");
        $this->cObj = $this->configurationManager->getContentObject();
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResultprototypeobject $queryResult
     * @return ObjectStorage
     */
    public function buildObjectStorageFromQuery($queryResult)
    {
        $storage = new ObjectStorage();

        foreach ($queryResult as $item) {
            $storage->attach($item);
        }

        return $storage;
    }

    protected function getFilterOptions($objectSwitch = '')
    {

        switch ($objectSwitch) {
            case 'positions':
                $objectSwitchedLocations = $this->locationRepository->findWithPositions('locations');
                break;
            case 'employers':
                // Das geht nicht! - dannach läuft der erste Ajax-Call immer ins
                // Leere...
                // Alle zu laden / ohne Filterung geht gar nicht => Error
                // Deshalb ist das asukommentiert für employers und es folgt der default case

                //$objectSwitchedLocations = $this->locationRepository->findWithEmployers('locations');
                //break;
            default:
                $objectSwitchedLocations = $this->locationRepository->findAll();
        }

        return [
            'types' => $this->typeRepository->findWithPositions('types'),
            'locations' => $objectSwitchedLocations,
            'regions' => $this->regionRepository->findByLocations($objectSwitchedLocations),
            'employments' => $this->employmentRepository->findWithPositions('employments'),
            'extras' => $this->extraRepository->findWithPositions('extras')
        ];
    }

    /**
     * @param array $recipient recipient of the email in the format array('recipient@domain.tld' => 'Recipient Name')
     * @param array $sender sender of the email in the format array('sender@domain.tld' => 'Sender Name')
     * @param string $subject subject of the email
     * @param string $templateName template name (UpperCamelCase)
     * @param array $variables variables to be passed to the Fluid view
     * @return boolean TRUE on success, otherwise false
     */
    protected function sendTemplateEmail(
        array $recipient,
        array $sender,
        $subject,
        $templateName,
        array $variables = array(),
        array $bcc = array(),
        array $replyTo = array()
    ) {
        /** @var \TYPO3\CMS\Fluid\View\StandaloneView $emailView */
        $emailView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');

        $extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        $templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($extbaseFrameworkConfiguration['view']['templateRootPaths'][0]);
        $templatePathAndFilename = $templateRootPath . 'Email/' . $templateName . '.html';
        $emailView->setTemplatePathAndFilename($templatePathAndFilename);
        $emailView->assignMultiple($variables);
        $emailBody = $emailView->render();

        /** @var $message \TYPO3\CMS\Core\Mail\MailMessage */
        $message = $this->objectManager->get('TYPO3\\CMS\\Core\\Mail\\MailMessage');
        $message->setTo($recipient)
            ->setFrom($sender)
            ->setSubject($subject);

        foreach ($bcc as $email => $name) {
            $message->addBcc($email, $name);
        }

        // Handle multiple and overwrite*
        foreach ($replyTo as $email => $name) {
            $message->setReplyTo($email, $name);
        }

        // Plain text example
        // $message->setBody($emailBody, 'text/plain');

        // HTML Email
        $message->setBody($emailBody, 'text/html');

        $message->send();
        return $message->isSent();
    }
}
