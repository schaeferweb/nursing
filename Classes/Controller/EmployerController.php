<?php
namespace Joekolade\Nursing\Controller;

use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
/***
 * This file is part of the "Pflegeberufe" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2017 Joe Schäfer <mail@schaefer-webentwicklung.de>, Schäfer – Büro für Webentwicklung
 ***/

/**
 * EmployerController
 */
class EmployerController extends \Joekolade\Nursing\Controller\AbstractController
{

    protected $OBJECT_SWITCH = 'employers';

    /**
     * positionRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\EmployerRepository
     * @inject
     */
    protected $employerRepository = null;

    public function initializeListAction()
    {
        $propertyMappingConfiguration = $this->arguments['filter']->getPropertyMappingConfiguration();
        $propertyMappingConfiguration->allowProperties('locs');
    }

    /**
     * action list
     *
     * @param \Joekolade\Nursing\Domain\Model\EmployerFilter $filter
     * @return void
     */
    public function listAction($filter = NULL)
    {
        // Title from flexform
        if ($filter == null) {
            $filter = new \Joekolade\Nursing\Domain\Model\EmployerFilter();

            $this->view->assign('prefilter', true);
        }

        $items = $this->loadFromCache($filter);

        $this->view->assignMultiple($items);
    }

    /**
     * show contact info
     *
     * @return void
     */
    public function showContactAction()
    {
        $employer = $this->employerRepository->findByUid($this->settings['employer']);
        $positions = $this->positionRepository->findByEmployer($employer);
        $this->view->assignMultiple([
            'employer' => $employer,
            'positions' => $positions
        ]);
    }

    /**
     * @param \Joekolade\Nursing\Domain\Model\Filter $filter
     * @return array
     */
    protected function loadFromCache($filter)
    {
        // Überprüfen ob Filter gesetzt sind.
        // und einen eindeutigen identifier für unseren cache generieren abhängig der filter.
        if (!$filter->isEmpty()) {
            $cacheString = $GLOBALS['TSFE']->id . '-' . $this->cObj->data['uid'] . '-' . $GLOBALS['TSFE']->sys_language_uid . '-' . $this->actionMethodName;
            // Add Filter to cache
            if (!empty($filter->getRegions())) {
                $cacheString .= '__regions';
                foreach ($filter->getRegions() as $regions) {
                    $cacheString .= '-' . $regions->getUid();
                }
            }
            if (!empty($filter->getLocations())) {
                $cacheString .= '__locations';
                foreach ($filter->getLocations() as $location) {
                    $cacheString .= '-' . $location->getUid();
                }
            }
        } else {
            $cacheString = $GLOBALS['TSFE']->id . '-' . $this->cObj->data['uid'] . '-' . $GLOBALS['TSFE']->sys_language_uid . '-' . $this->actionMethodName;
        }
        $cacheIdentifier = md5(
            $cacheString
        );

        if ($this->cacheInstance->has($cacheIdentifier)) {
            // Cache vorhanden
            $items = $this->cacheInstance->get($cacheIdentifier);
        } else {

            $items = $this->getFilterOptions($this->OBJECT_SWITCH);

            $employers = $this->employerRepository->findByFilter($filter);

            $items = array_merge($items, [
                'title' => $this->settings['title'],
                'filter' => $filter,
                'employers' => $employers,
                'isAjax' => $_REQUEST['type'] === $this->settings['employerAjaxpagetype'] || false
            ]);

            $this->cacheInstance->set($cacheIdentifier, $items, ['nursingEmploymentFilter']);
        }
        return $items;
    }
}
