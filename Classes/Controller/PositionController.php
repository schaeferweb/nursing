<?php
namespace Joekolade\Nursing\Controller;

use Joekolade\Nursing\Domain\Form\QuickApplicationForm;
use Joekolade\Nursing\Domain\Model\Position;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
/***
 * This file is part of the "Pflegeberufe" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2017 Joe Schäfer <mail@schaefer-webentwicklung.de>, Schäfer – Büro für Webentwicklung
 ***/

/**
 * PositionController
 */
class PositionController extends \Joekolade\Nursing\Controller\AbstractController
{

    protected $OBJECT_SWITCH = 'positions';

    /**
     * positionRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\PositionRepository
     * @inject
     */
    protected $positionRepository = null;

    public function initializeListAction()
    {
        $propertyMappingConfiguration = $this->arguments['filter']->getPropertyMappingConfiguration();
        $propertyMappingConfiguration->allowProperties('locs');
    }

    /**
     * action list
     *
     * @param \Joekolade\Nursing\Domain\Model\Filter $filter
     * @return void
     */
    public function listAction($filter = NULL)
    {
        // Title from flexform
        if ($filter == null) {
            $filter = new \Joekolade\Nursing\Domain\Model\Filter();
        }

        $pluginInUse = false;

        // Preselection active in plugin?
        if (!empty($this->settings['preTypes'])) {
            $filter->setTypes($this->buildObjectStorageFromQuery($this->typeRepository->findByUidList($this->settings['preTypes'])));
            $pluginInUse = true;
        }
        if (!empty($this->settings['preLocations'])) {
            $filter->setLocations($this->buildObjectStorageFromQuery($this->locationRepository->findByUidList($this->settings['preLocations'])));
            $pluginInUse = true;
        }
        if (!empty($this->settings['preRegions'])) {
            $filter->setRegions($this->buildObjectStorageFromQuery($this->regionRepository->findByUidList($this->settings['preRegions'])));
            $pluginInUse = true;
        }
        if (!empty($this->settings['preEmployments'])) {
            $filter->setEmployments($this->buildObjectStorageFromQuery($this->employmentRepository->findByUidList($this->settings['preEmployments'])
            ));
            $pluginInUse = true;
        }
        if (!empty($this->settings['preExtras'])) {
            $filter->setExtras($this->buildObjectStorageFromQuery($this->extraRepository->findByUidList($this->settings['preExtras'])));
            $pluginInUse = true;
        }

        $items = $this->loadFromCache($filter);

        $this->view->assignMultiple($items);

        # switch for seo plugin
        $this->view->assign('pluginInUse', $pluginInUse);
    }

    /**
     * action ajaxList
     *
     * @param array $items
     */
    public function ajaxList($items)
    {
        $this->view->assignMultiple($items);
    }

    /**
     * action show
     *
     * @param \Joekolade\Nursing\Domain\Model\Position $position
     * @param \Joekolade\Nursing\Domain\Form\QuickApplicationForm $quickApplicationForm
     * @dontvalidate $quickApplicationForm
     * @return void
     */
    public function showAction(
        \Joekolade\Nursing\Domain\Model\Position $position,
        QuickApplicationForm $quickApplicationForm = null
    ) {
        if(!$quickApplicationForm) {
            $this->view->assign('quickApplicationForm', new QuickApplicationForm());
        } else {
            $this->view->assign('quickApplicationForm', $quickApplicationForm);
        }

        $this->view->assign('position', $position);

        // DEV MODE
        $this->view->assign('dev', $this->settings['devMode']);
    }

    /**
     * action searchbox
     *
     * @return void
     */
    public function searchboxAction()
    {
        /*
         * @var \TYPO3\CMS\Core\Resource\FileRepository $fileRepository
         */

        $fileRepository = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\FileRepository::class);
        $img = $fileRepository->findFileReferenceByUid($this->settings['media']);

        $items = $this->getFilterOptions($this->OBJECT_SWITCH);

        $this->view->assignMultiple($items);

        $this->view->assignMultiple([
            'searchbox' => 1,
            'image' => $img,
            'filter' => new \Joekolade\Nursing\Domain\Model\Filter(),
        ]);
    }

    /**
     * @param \Joekolade\Nursing\Domain\Model\Filter $filter
     * @return array
     */
    protected function loadFromCache($filter)
    {
        // Überprüfen ob Filter gesetzt sind.
        // und einen eindeutigen identifier für unseren cache generieren abhängig der filter.
        if (!$filter->isEmpty()) {
            $cacheString = $GLOBALS['TSFE']->id . '-' . $this->cObj->data['uid'] . '-' . $GLOBALS['TSFE']->sys_language_uid . '-' . $this->actionMethodName;
            // Add Filter to cache
            if (!empty($filter->getTypes())) {
                $cacheString .= '__types';
                foreach ($filter->getTypes() as $type) {
                    $cacheString .= '-' . $type->getUid();
                }
            }
            if (!empty($filter->getRegions())) {
                $cacheString .= '__regions';
                foreach ($filter->getRegions() as $regions) {
                    $cacheString .= '-' . $regions->getUid();
                }
            }
            if (!empty($filter->getLocations())) {
                $cacheString .= '__locations';
                foreach ($filter->getLocations() as $location) {
                    $cacheString .= '-' . $location->getUid();
                }
            }
            if (!empty($filter->getEmployments())) {
                $cacheString .= '__employments';
                foreach ($filter->getEmployments() as $employment) {
                    $cacheString .= '-' . $employment->getUid();
                }
            }
            if (!empty($filter->getExtras())) {
                $cacheString .= '__extras';
                foreach ($filter->getExtras() as $extra) {
                    $cacheString .= '-' . $extra->getUid();
                }
            }
        } else {
            $cacheString = $GLOBALS['TSFE']->id . '-' . $this->cObj->data['uid'] . '-' . $GLOBALS['TSFE']->sys_language_uid . '-' . $this->actionMethodName;
        }
        $cacheIdentifier = md5(
            $cacheString
        );
        if ($this->cacheInstance->has($cacheIdentifier)) {
            // Cache vorhanden
            $items = $this->cacheInstance->get($cacheIdentifier);
        } else {

            $items = $this->getFilterOptions($this->OBJECT_SWITCH);

            $positions = $this->positionRepository->findByFilter($filter);
            $items = array_merge($items, [
                'title' => $this->settings['title'],
                'positions' => $positions,
                'filter' => $filter,
                'isAjax' => $_REQUEST['type'] === $this->settings['ajaxpagetype'] || false
            ]);
            $this->cacheInstance->set($cacheIdentifier, $items, ['nursingFilter']);
        }

        return $items;
    }

    /**
     * sendQuickApplication
     *
     * @param QuickApplicationForm $quickApplicationForm
     */
    public function sendQuickApplicationAction(QuickApplicationForm $quickApplicationForm)
    {

        // Is honeypot filled?
        if (isset($_GET['email2']) && $_GET['email2'] !== '') {
            die('No robots allowed here!');
        }

        // validation is done via QuickApplicationForm class

        if ($this->request->hasArgument('position')) {
            /** @var Position $position */
            $position = $this->positionRepository->findByUid($this->request->getArgument('position'));
        }

        // STOP, if no email is given
        if ($position->getEmployer()->getEmail() == '') {
            die('Für den Arbeitgeber wurde noch keine E-Mail hinterlegt.');
        }

        $variables = array(
            'form' => $quickApplicationForm,
            'position' => $position,
            'settings' => $this->settings,
            'bcc' => $this->settings['applicationEmail']['bccEmail'],
        );
        $bcc = array(
            $this->settings['applicationEmail']['bccEmail'] => $this->settings['applicationEmail']['bccName']
        );
        $replyTo = array(
            $quickApplicationForm->getEmail() => $quickApplicationForm->getApplicant()
        );
        $receiver = array(
            $position->getEmployer()->getEmail() => $position->getEmployer()->getTitle()
        );
        $subject = $this->settings['applicationEmail']['subject'] . ' [' . $position->getTitle() . ']';

        if ($this->settings['applicationEmail']['overRideReceiver']) {
            $receiver = array(
                $this->settings['applicationEmail']['overRideReceiver']['email'] => $this->settings['applicationEmail']['overRideReceiver']['name']
            );
            $subject = $this->settings['applicationEmail']['overRideReceiver']['subjectPrefix'] . $subject;
        }

        // Send email

        $this->sendTemplateEmail(
            $receiver,
            array(
                $this->settings['applicationEmail']['senderEmail'] => $this->settings['applicationEmail']['senderName']
            ), // $sender
            $subject,
            'QuickApplication', // $templateName
            $variables,
            $bcc,
            $replyTo
        );

        $this->view->assignMultiple(array(
            'form' => $quickApplicationForm,
            'position' => $position,
            'bcc' => $this->settings['applicationEmail']['bccEmail'],
            'isAjax' => $_REQUEST['type'] === $this->settings['formAjaxpagetype'] || false
        ));

    }
}
