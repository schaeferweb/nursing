<?php

namespace Joekolade\Nursing\ViewHelpers;

class GeoArrayViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * Beschreibung der Methode
     *
     * @param array $employers
     * @return string JS Array employers
     */
    public function render($employers)
    {
        $jsontext = "[";
        foreach ($employers as $key => $employer) {
            $jsontext .= "{geo: [" . addslashes($employer->getGeolocation()) . "], desc: '" . addslashes($employer->getTitle() . $employer->getDescription()) . "'},";
        }
        $jsontext = substr_replace($jsontext, '', -1); // to get rid of extra comma
        $jsontext .= "]";

        return $jsontext;
    }

    public function buildTitle($employer)
    {
//        return
//            '<div class="tx-nursing__item-header"><header><h2 itemprop="title">'
//
//						{employer.title}
//					</f:link.page>
//				</h2>
//
//				<p itemprop="employerLocation" itemscope itemtype="http://schema.org/Place">
//		<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
//			<span itemprop="addressLocality">{employer.geoaddress}</span>
//		</span>
//				</p>
//
//			</header>
//		</div>
//        ;
    }
}
