<?php

namespace Joekolade\Nursing\ViewHelpers;

class MailUrlViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * As this ViewHelper renders HTML, the output must not be escaped.
     *
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Beschreibung der Methode
     *
     * @param string $mail
     * @param string $class
     * @param Joekolade\Nursing\Domain\Model\Position $position
     * @return string JS Array employers
     */
    public function render($mail = '', $class = '', $position = null)
    {
        $mailurl = 'mailto:';
        $mailurl .= $mail;
        if ($position) {
            $mailurl .= '?subject=' . htmlspecialchars($position->getTitle());
        }

        $maillink = '<a class="' . $class . '" href="' . $mailurl . '">' . $this->renderChildren() . '</a>';

        return $maillink;
    }
}
