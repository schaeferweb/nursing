<?php

namespace Joekolade\Nursing\Domain\Form;

class QuickApplicationForm extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * @var string
     * @validate NotEmpty
     */
    protected $applicant = '';

    /**
     * @var string
     */
    protected $telephone = '';

    /**
     * @var string
     * @validate NotEmpty
     * @validate EmailAddress
     */
    protected $email = '';

    /**
     * @var string
     * @validate NotEmpty
     */
    protected $application = '';

    /**
     * @var bool
     * @validate NotEmpty
     */
    protected $legals = '';

    /**
     * @return string
     */
    public function getApplicant(): string
    {
        return $this->applicant;
    }

    /**
     * @param string $applicant
     */
    public function setApplicant(string $applicant): void
    {
        $this->applicant = $this->_cleanString($applicant);
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone(string $telephone): void
    {
        $this->telephone = $this->_cleanString($telephone);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $this->_cleanString($email);
    }

    /**
     * @return string
     */
    public function getApplication(): string
    {
        return $this->application;
    }

    /**
     * @param string $application
     */
    public function setApplication(string $application): void
    {
        $this->application = $this->_cleanString($application);
    }

    /**
     * @return bool
     */
    public function isLegals(): bool
    {
        return $this->legals;
    }

    /**
     * @param bool $legals
     */
    public function setLegals(bool $legals): void
    {
        $this->legals = $legals;
    }



    protected function _cleanString($string)
    {
        return htmlspecialchars(strip_tags(trim($string)));
    }

}
