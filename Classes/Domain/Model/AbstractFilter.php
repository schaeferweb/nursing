<?php
namespace Joekolade\Nursing\Domain\Model;

/***
 *
 * This file is part of the "Pflegeberufe" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Joe Schäfer <mail@schaefer-webentwicklung.de>, Schäfer – Büro für Webentwicklung
 *
 ***/

/**
 * AbstractFilter
 */
class AbstractFilter extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * is filter empty
     *
     * @return bool
     */
    public function isEmpty()
    {
        if ($this->getLocations()->count() || $this->getRegions()->count()) {
            return false;
        }
        return true;
    }
}
