<?php
namespace Joekolade\Nursing\Domain\Model;

/***
 *
 * This file is part of the "Pflegeberufe" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Joe Schäfer <mail@schaefer-webentwicklung.de>, Schäfer – Büro für Webentwicklung
 *
 ***/

/**
 * EmployerFilter
 */
class EmployerFilter extends \Joekolade\Nursing\Domain\Model\AbstractFilter
{
    /**
     * locs
     *
     * @var string
     */
    protected $locs = '';

    /**
     * locations
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Joekolade\Nursing\Domain\Model\Location>
     */
    protected $locations = null;

    /**
     * regions
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Joekolade\Nursing\Domain\Model\Region>
     */
    protected $regions = null;

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->locations = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->regions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the locs
     *
     * @return string $locs
     */
    public function getLocs()
    {
        return $this->locs;
    }

    /**
     * Sets the locs
     *
     * @param string $locs
     * @return void
     */
    public function setLocs($locs)
    {
        $this->locs = $locs;
    }

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Adds a Location
     *
     * @param \Joekolade\Nursing\Domain\Model\Location $location
     * @return void
     */
    public function addLocation(\Joekolade\Nursing\Domain\Model\Location $location)
    {
        $this->locations->attach($location);
    }

    /**
     * Removes a Location
     *
     * @param \Joekolade\Nursing\Domain\Model\Location $locationToRemove The Location to be removed
     * @return void
     */
    public function removeLocation(\Joekolade\Nursing\Domain\Model\Location $locationToRemove)
    {
        $this->locations->detach($locationToRemove);
    }

    /**
     * Returns the locations
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Joekolade\Nursing\Domain\Model\Location> $locations
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Sets the locations
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Joekolade\Nursing\Domain\Model\Location> $locations
     * @return void
     */
    public function setLocations(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $locations)
    {
        $this->locations = $locations;
    }

    /**
     * Adds a Region
     *
     * @param \Joekolade\Nursing\Domain\Model\Region $region
     * @return void
     */
    public function addRegion(\Joekolade\Nursing\Domain\Model\Region $region)
    {
        $this->regions->attach($region);
    }

    /**
     * Removes a Region
     *
     * @param \Joekolade\Nursing\Domain\Model\Region $regionToRemove The Region to be removed
     * @return void
     */
    public function removeRegion(\Joekolade\Nursing\Domain\Model\Region $regionToRemove)
    {
        $this->regions->detach($regionToRemove);
    }

    /**
     * Returns the regions
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Joekolade\Nursing\Domain\Model\Region> $regions
     */
    public function getRegions()
    {
        return $this->regions;
    }

    /**
     * Sets the regions
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Joekolade\Nursing\Domain\Model\Region> $regions
     * @return void
     */
    public function setRegions(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $regions)
    {
        $this->regions = $regions;
    }
}
