<?php
namespace Joekolade\Nursing\Domain\Model;

/***
 *
 * This file is part of the "Pflegeberufe" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Joe Schäfer <mail@schaefer-webentwicklung.de>, Schäfer – Büro für Webentwicklung
 *
 ***/

/**
 * Employer
 */
class Employer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * logo
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $logo = null;

    /**
     * address
     *
     * @var string
     */
    protected $address = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * detailpid
     *
     * @var string
     */
    protected $detailpid = 0;

    /**
     * geoaddress
     *
     * @var string
     */
    protected $geoaddress = '';

    /**
     * geolocation
     *
     * @var string
     */
    protected $geolocation = '';

    /**
     * telephone
     *
     * @var string
     */
    protected $telephone = '';

    /**
     * website
     *
     * @var string
     */
    protected $website = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * location
     *
     * @var \Joekolade\Nursing\Domain\Model\Location
     */
    protected $location = null;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the logo
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Sets the logo
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     * @return void
     */
    public function setLogo(\TYPO3\CMS\Extbase\Domain\Model\FileReference $logo)
    {
        $this->logo = $logo;
    }

    /**
     * Returns the address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the address
     *
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the detailpid
     *
     * @return string detailpid
     */
    public function getDetailpid()
    {
        return $this->detailpid;
    }

    /**
     * Sets the detailpid
     *
     * @param int $detailpid
     * @return void
     */
    public function setDetailpid($detailpid)
    {
        $this->detailpid = $detailpid;
    }

    /**
     * Returns the geolocation
     *
     * @return string $geolocation
     */
    public function getGeolocation()
    {
        return $this->geolocation;
    }

    /**
     * Sets the geolocation
     *
     * @param string $geolocation
     * @return void
     */
    public function setGeolocation($geolocation)
    {
        $this->geolocation = $geolocation;
    }

    /**
     * Returns the geoaddress
     *
     * @return string $geoaddress
     */
    public function getGeoaddress()
    {
        return $this->geoaddress;
    }

    /**
     * Sets the geoaddress
     *
     * @param string $geoaddress
     * @return void
     */
    public function setGeoaddress($geoaddress)
    {
        $this->geoaddress = $geoaddress;
    }

    /**
     * Returns the location
     *
     * @return \Joekolade\Nursing\Domain\Model\Location $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location
     *
     * @param \Joekolade\Nursing\Domain\Model\Location $location
     * @return void
     */
    public function setLocation(\Joekolade\Nursing\Domain\Model\Location $location)
    {
        $this->location = $location;
    }

    /**
     * Returns the telephone
     *
     * @return string $telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Sets the telephone
     *
     * @param string $telephone
     * @return void
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * Returns the website
     *
     * @return string $website
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Sets the website
     *
     * @param string $website
     * @return void
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}
