<?php
namespace Joekolade\Nursing\Domain\Repository;

/***
 *
 * This file is part of the "Pflegeberufe" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Joe Schäfer <mail@schaefer-webentwicklung.de>, Schäfer – Büro für Webentwicklung
 *
 ***/

/**
 * The repository for Employers
 */
class EmployerRepository extends AbstractRepository
{
    /**
     * locationRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\LocationRepository
     * @inject
     */
    protected $locationRepository = null;

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'detailpid' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
        'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
        //'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * @param \Joekolade\Nursing\Domain\Model\EmployerFilter $filter
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByFilter($filter = null)
    {
        $query = $this->createQuery();
        // Constraints initialisieren
        $constraints = [];
        if ($filter) {
            // $query->in('uid', $filteredArticles);
            // Locations
            if (count($filter->getLocations())) {
                $optMatch = [];
                foreach ($filter->getLocations() as $option) {
                    $optMatch[] = $query->equals('location', $option);
                }
                if (!empty($optMatch)) {
                    $constraints[] = $query->logicalOr($optMatch);
                }
            }
            // Regions
            if (count($filter->getRegions())) {
                $optMatch = [];
                foreach ($filter->getRegions() as $region) {
                    foreach ($this->locationRepository->findByRegion($region) as $option) {
                        $optMatch[] = $query->equals('location', $option);
                    }
                }
                if (!empty($optMatch)) {
                    $constraints[] = $query->logicalOr($optMatch);
                }
            }
        }
        if (count($constraints)) {
            $query->matching($query->logicalAnd($constraints));
        }
        //        if($offset) $query->setOffset($offset);
        //        if($limit) $query->setLimit($limit);
        return $query->execute();
    }
}
