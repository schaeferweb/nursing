<?php

namespace Joekolade\Nursing\Domain\Repository;

/***
 * This file is part of the "Pflegeberufe" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *  (c) 2017 Joe Schäfer <mail@schaefer-webentwicklung.de>, Schäfer – Büro für Webentwicklung
 ***/

/**
 * The repository for Regions
 */
class RegionRepository extends AbstractRepository
{

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * @param $locations
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByLocations($locations)
    {
        $query = $this->createQuery();

        $restrictions = [];
        foreach ($locations as $location) {
            /** @var \Joekolade\Nursing\Domain\Model\Location $location */
            $regions = $location->getRegions();
            if (count($regions)) {
                foreach ($regions as $region) {
                    $restrictions[] = $query->equals('uid', $region->getUid());
                }
            }
        }
        if (count($restrictions)) {
            $query->matching($query->logicalOr($restrictions));
        } else {
            $query->matching($query->logicalOr(0));
        }

        return $query->execute();
    }
}
