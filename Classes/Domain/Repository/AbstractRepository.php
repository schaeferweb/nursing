<?php
namespace Joekolade\Nursing\Domain\Repository;

/***
 *
 * This file is part of the "Pflegeberufe" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Joe Schäfer <mail@schaefer-webentwicklung.de>, Schäfer – Büro für Webentwicklung
 *
 ***/

use TYPO3\CMS\Extbase\Persistence\Generic\Qom\OrInterface;

/**
 * The abstract repository
 */
class AbstractRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * positionRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\PositionRepository
     * @inject
     */
    protected $positionRepository = null;

    /**
     * employerRepository
     *
     * @var \Joekolade\Nursing\Domain\Repository\EmployerRepository
     * @inject
     */
    protected $employerRepository = null;

    /**
     * @param string $list
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByUidList($list)
    {
        $query = $this->createQuery();
        // Constraints initialisieren
        $constraints = [];

        if (empty($list)) {
            // do nothing special
        } else {
            $optMatch = [];
            foreach (explode(',', $list) as $uid) {
                $optMatch[] = $query->equals('uid', $uid);
            }
            $constraints[] = $query->logicalOr($optMatch);
        }

        if (!empty($constraints)) {
            $query->matching($query->logicalAnd($constraints));
        }

        return $query->execute();
    }

    /**
     * Find only objects that have an active position signed to them
     *
     * @param string $objectsAsString
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findWithPositions($objectsAsString = '')
    {
        $query = $this->createQuery();

        $query->matching($this->getRestrictedByPositions($query, $objectsAsString));

        return $query->execute();
    }

    /**
     * Find only objects that have an active employer signed to them
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findWithEmployers()
    {

        $query = $this->createQuery();

        $query->matching($this->getRestrictedByEmployers($query));

        return $query->execute();
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @param string $objectsAsString
     * @return OrInterface
     */
    protected function getRestrictedByPositions($query, $objectsAsString = '')
    {

        $positions = $this->positionRepository->findAll();

        $restrictions = [];

        foreach ($positions as $position) {
            /** @var \Joekolade\Nursing\Domain\Model\Position $position */
            $objects = [];

            switch ($objectsAsString) {
                case 'types':
                    $objects[] = $position->getTypes();
                    break;

                case 'employments':
                    foreach ($position->getEmployments() as $employment) {
                        $objects[] = $employment;
                    }
                    break;
                case 'extras':
                    foreach ($position->getExtras() as $extra) {
                        $objects[] = $extra;
                    }
                    break;
                case 'locations':
                default:
                    $objects[] = $position->getLocation();
            }

            if (count($objects)) {
                foreach ($objects as $object) {
                    $restrictions[] = $query->equals('uid', $object->getUid());
                }
            }
        }
        if (count($restrictions)) {
            return $query->logicalOr($restrictions);
        }

        return $query->logicalOr(0);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @return OrInterface
     */
    protected function getRestrictedByEmployers($query)
    {

        $employers = $this->employerRepository->findAll();

        $restrictions = [];

        foreach ($employers as $employer) {
            /** @var \Joekolade\Nursing\Domain\Model\Employer $employer */
            if ($employer->getLocation()) {
                $restrictions[] = $query->equals('uid', $employer->getLocation()->getUid());
            }
        }
        if (count($restrictions)) {
            return $query->logicalOr($restrictions);
        }

        return $query->logicalOr(0);
    }
}
