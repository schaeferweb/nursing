<?php

namespace Joekolade\Nursing\Hook;

/**
 * Hook to process updated records.
 */
class DataMapHook
{

    /**
     * Hook to add latitude and longitude to locations.
     *
     * @param string $action The action to perform, e.g. 'update'.
     * @param string $table The table affected by action, e.g. 'fe_users'.
     * @param int $uid The uid of the record affected by action.
     * @param array $modifiedFields The modified fields of the record.
     *
     * @return void
     */
    public function processDatamap_postProcessFieldArray( // @codingStandardsIgnoreLine
        $action,
        $table,
        $uid,
        array &$modifiedFields
    ) {


        if (!$this->processGeocoding($table, $action, $modifiedFields)) {
            return;
        }

        /** @var $extbaseObjectManager \TYPO3\CMS\Extbase\Object\ObjectManager */
        $extbaseObjectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        /** @var $positionRepository \Joekolade\Nursing\Domain\Repository\PositionRepository */
        $positionRepository = $extbaseObjectManager->get('Joekolade\Nursing\Domain\Repository\PositionRepository');
        /** @var $position \Joekolade\Nursing\Domain\Model\Position */
        $position = $positionRepository->findByUid($uid);


        /** @var $location \Joekolade\Nursing\Domain\Model\Location */
        $location = $position->getLocation();

        $address = '';
        if ($position->getGeostreet() || isset($modifiedFields['geostreet'])) {
            $address .= (isset($modifiedFields['geostreet']) && $modifiedFields['geostreet'] != '') ? $modifiedFields['geostreet'] : $position->getGeostreet();
            $address .= '+';
        }
        $address .= $location->getZip() . ' ' . $location->getTitle();

        $modifiedFields['geolocation'] = $this->getCoordinates($address);
    }

    /**
     * Check whether to execute hook or not.
     *
     * @param string $table
     * @param string $action
     * @param array $modifiedFields
     *
     * @return bool
     */
    protected function processGeocoding($table, $action, $modifiedFields)
    {
        // Do not process if foreign table, unintended action,
        // or fields were changed explicitly.
        if (
            $table !== 'tx_nursing_domain_model_position'
            ||
            $action !== 'update'
        ) {
            return false;
        }

        return true;
    }

    /**
     * geocoord function from
     * https://colinyeoh.wordpress.com/2013/02/12/simple-php-function-to-get-coordinates-from-address-through-google-services/
     *
     * @param string $address
     * @return string
     */
    function getCoordinates($address)
    {

        $address = str_replace(" ", "+",
            $address); // replace all the white space with "+" sign to match with google search pattern

        $url = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";

        $response = file_get_contents($url);

        $json = json_decode($response, true); //generate array object from the response from the web

        if (count($json['results'])) {
            return ($json['results'][0]['geometry']['location']['lat'] . "," . $json['results'][0]['geometry']['location']['lng']);
        }
        return false;
    }
}