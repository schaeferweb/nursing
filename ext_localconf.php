<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Joekolade.Nursing',
            'List',
            [
                'Position' => 'list, show, sendQuickApplication',
            ],
            // non-cacheable actions
            [
                'Position' => 'list, show, sendQuickApplication'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Joekolade.Nursing',
            'Stage',
            [
                'Position' => 'searchbox'
            ],
            // non-cacheable actions
            [
                'Position' => '',
                'Employer' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Joekolade.Nursing',
            'Employerlist',
            [
                'Employer' => 'list'
            ],
            // non-cacheable actions
            [
                'Position' => '',
                'Employer' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Joekolade.Nursing',
            'Employerdetails',
            [
                'Employer' => 'showContact'
            ],
            // non-cacheable actions
            [
                'Position' => '',
                'Employer' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    list {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('nursing') . 'Resources/Public/Icons/user_plugin_list.svg
                        title = LLL:EXT:nursing/Resources/Private/Language/locallang_db.xlf:tx_nursing_domain_model_list
                        description = LLL:EXT:nursing/Resources/Private/Language/locallang_db.xlf:tx_nursing_domain_model_list.description
                        tt_content_defValues {
                            CType = list
                            list_type = nursing_list
                        }
                    }
                    stage {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('nursing') . 'Resources/Public/Icons/user_plugin_stage.svg
                        title = LLL:EXT:nursing/Resources/Private/Language/locallang_db.xlf:tx_nursing_domain_model_stage
                        description = LLL:EXT:nursing/Resources/Private/Language/locallang_db.xlf:tx_nursing_domain_model_stage.description
                        tt_content_defValues {
                            CType = list
                            list_type = nursing_stage
                        }
                    }
                    employerlist {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('nursing') . 'Resources/Public/Icons/user_plugin_employerlist.svg
                        title = LLL:EXT:nursing/Resources/Private/Language/locallang_db.xlf:tx_nursing_domain_model_employerlist
                        description = LLL:EXT:nursing/Resources/Private/Language/locallang_db.xlf:tx_nursing_domain_model_employerlist.description
                        tt_content_defValues {
                            CType = list
                            list_type = nursing_employerlist
                        }
                    }
                    employerdetails {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('nursing') . 'Resources/Public/Icons/user_plugin_employerdetails.svg
                        title = LLL:EXT:nursing/Resources/Private/Language/locallang_db.xlf:tx_nursing_domain_model_employerdetails
                        description = LLL:EXT:nursing/Resources/Private/Language/locallang_db.xlf:tx_nursing_domain_model_employerdetails.description
                        tt_content_defValues {
                            CType = list
                            list_type = nursing_employerdetails
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder


//$signalSlotDispatcher = TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
//$signalSlotDispatcher->connect(
//    \TYPO3\CMS\Core\DataHandling\DataHandler::class
//    'DoSomeThing',
//    YourNameSpace\YourExtension\Slot\DataMapHook::class,
//    'processDatamap_postProcessFieldArray'
//);
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][$_EXTKEY]
    = 'Joekolade\Nursing\Hook\DataMapHook';

# Cs Seo
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:nursing/Configuration/TSconfig/Extensions/cs_seo_page.ts">'
);

// Caching framework
if( !is_array($GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations'][$_EXTKEY] ) ) {
    $GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations'][$_EXTKEY] = array();
}
// Hier ist der entscheidende Punkt! Es ist der Cache von Variablen gesetzt!
if( !isset($GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations'][$_EXTKEY]['frontend'] ) ) {
    $GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations'][$_EXTKEY]['frontend'] = 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend';
}
// Wie lange soll der Cache haltbar sein? (1 Stunde)
if( !isset($GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations'][$_EXTKEY]['options'] ) ) {
    $GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations'][$_EXTKEY]['options'] = array('defaultLifetime' => 3600);
}
if( !isset($GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations'][$_EXTKEY]['groups'] ) ) {
    $GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations'][$_EXTKEY]['groups'] = array('pages');
}
