tx_csseo {
	1 = tx_nursing_domain_model_position
	1 {
		enable = GP:tx_nursing_list|position
		fallback {
			title = title
			description = description
		}

		evaluation {
			getParams = &tx_nursing_list[controller]=Position&tx_nursing_list[action]=show&tx_nursing_list[position]=|
			detailPid = 33
		}
	}

	2 = tx_nursing_domain_model_employer
	2 {
		enable = GP:tx_nursing_list|employer
		fallback {
			title = title
			description = description
		}

		evaluation {
			getParams = &tx_nursing_list[controller]=Employer&tx_nursing_list[action]=show&tx_nursing_list[employer]=|
			detailPid = 30
		}
	}

}
