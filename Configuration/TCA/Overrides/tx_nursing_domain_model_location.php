<?php
$tca = [
    'ctrl' => [
        'label' => 'title',
        'label_alt' => 'zip',
        'label_alt_force' => 1
    ],

    'columns' => [
    'regions' => [
        'config' => [
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'tx_nursing_domain_model_region',
            'foreign_table' => 'tx_nursing_domain_model_region',
            'MM' => 'tx_nursing_location_region_mm',
            'size' => 10,
            'autoSizeMax' => 30,
            'minitems' => 0,
            'maxitems' => 20,
            'multiple' => 0,
            'wizards' => array(
                'suggest' => array(
                    'type' => 'suggest',
                ),
            ),
        ],
    ],
],
];
$GLOBALS['TCA']['tx_nursing_domain_model_location'] = array_replace_recursive($GLOBALS['TCA']['tx_nursing_domain_model_location'],
    $tca);
