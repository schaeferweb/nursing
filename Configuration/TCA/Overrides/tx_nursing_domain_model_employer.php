<?php

$tca = [
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, logo, description, email, location, geoaddress, geolocation, address, detailpid, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'detailpid' => [
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
                'fieldControl' => array(
                    'linkPopup' => array(
                        'options' => array(
                            'blindLinkOptions' => 'mail,url,file,folder',
                        ),
                    ),
                ),
            ]
        ],
        'geolocation' => [
            //'config' => ['readOnly' => true],
        ],
        'location' => [
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_nursing_domain_model_location',
                'foreign_table' => 'tx_nursing_domain_model_location',
                'renderType' => '',
                //'enableMultiSelectFilterTextfield' => true,
                'minitems' => 0,
                'maxitems' => 1,

                'wizards' => array(
                    'suggest' => array(
                        'type' => 'suggest',
                    ),
                ),
//                'items' => [
//                    ['--- Bitte auswählen ---', 0]
//                ],
            ],
        ],
    ]
];
$GLOBALS['TCA']['tx_nursing_domain_model_employer'] = array_replace_recursive($GLOBALS['TCA']['tx_nursing_domain_model_employer'],
    $tca);
