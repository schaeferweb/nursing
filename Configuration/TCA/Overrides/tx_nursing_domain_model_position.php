<?php

$tca = [
    'columns' => [
        'types' => [
            'config' => [
                'renderType' => 'selectMultipleSideBySide',
                'enableMultiSelectFilterTextfield' => true,
                'minitems' => 1,
                'maxitems' => 1,
            ],
        ],
        'location' => [
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'renderType' => '',
                'foreign_table' => 'tx_nursing_domain_model_location',
                'allowed' => 'tx_nursing_domain_model_location',
                'minitems' => 1,
                'maxitems' => 1,
            ],
        ],
        'images' => [
            'config' => [
                'cropVariants' => [
                    'default' => [
                        'title' => 'Position: Stellenbild',
                        'allowedAspectRatios' => [
                            '4:3' => [
                                'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.4_3',
                                'value' => 4 / 3
                            ],
                        ],
                        'selectedRatio' => '4:3',
                        'cropArea' => [
                            'x' => 0.0,
                            'y' => 0.0,
                            'width' => 1.0,
                            'height' => 1.0,
                        ]
                    ]
                ]
            ]
        ],

    ],
];
$GLOBALS['TCA']['tx_nursing_domain_model_position'] = array_replace_recursive($GLOBALS['TCA']['tx_nursing_domain_model_position'],
    $tca);
