plugin.tx_nursing {
	view {
		# cat=plugin.tx_nursing/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:nursing/Resources/Private/Templates/
		# cat=plugin.tx_nursing/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:nursing/Resources/Private/Partials/
		# cat=plugin.tx_nursing/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:nursing/Resources/Private/Layouts/
	}

	persistence {
		# cat=plugin.tx_nursing//a; type=string; label=Default storage PID
		storagePid = 3
	}

	settings {
		listPID = 33
		employerListPID = 30
		ajaxpagetype = 6661
		employerAjaxpagetype = 6662
		formAjaxpagetype = 6663

		coordfetchmaximum = 50

		list {
			showRegions = 0
			showEmployer = 1
		}


		features {
			employerLinkInPositionList = 0
			lightbox {
				enable = 1
				# Default use sd-lightbox
				css-class = sd-lightbox
				rel-attribute = sd-lightbox
			}

			useQuickApplicationForms = 0
			usePowermailForQuickApplication = 0
		}

		legalsPageId = 17

		applicationEmail {

			powermail {
				useFormPid = 0
			}

			subject = Kurze Bewerbung

			bccName = Werbeagentur Fetscher
			bccEmail = print@werbeagentur-fetscher.de

			senderName = Mein Pflegeberuf
			senderEmail = noreply@mein-pflegeberuf.de

			overRideReceiver = 1
			overRideReceiver {
				name = TEST (Mein Pflegeberuf)
				email = print@werbeagentur-fetscher.de
				subjectPrefix = 'TEST - '
			}
		}
	}
}

// ext:scriptmerger
//
plugin.tx_scriptmerger {
	css {
		minify {
			ignore = \?,\.min\.,unpkg\.com
		}

		compress {
			ignore = \?,unpkg\.com
		}

		merge {
			ignore = \?,unpkg\.com
		}
	}
}

// ext:jh_maqnificpopup
//
plugin.tx_jhmagnificpopup.magnificpopup.support.user = nursing-lightbox
