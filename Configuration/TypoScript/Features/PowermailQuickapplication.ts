[globalVar = GP:tx_nursing_list|position > 0]
    TEMP._postion_from_db = CONTENT
    TEMP._postion_from_db {
        table = tx_nursing_domain_model_position
        select {
            pidInList = {$plugin.tx_nursing.persistence.storagePid}
            uidInList = {GP:tx_nursing_list|position}
            uidInList.insertData = 1
            recursive = 9
        }

        renderObj = TEXT
    }

    plugin.tx_powermail.settings.setup.prefill {
        tx_nursing_stellenbezeichnung < TEMP._postion_from_db
        tx_nursing_stellenbezeichnung.renderObj.field = title
    }

[global]

lib.tx_nursing_quick_application = COA_INT
lib.tx_nursing_quick_application {

    5 = TEXT
    5.value =
    5.dataWrap = {field: title} |

    10 = USER
    10 {

        userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
        extensionName = Powermail
        vendorName = In2code
        pluginName = Pi1
        controller = Form
        action = Form
        settings < plugin.tx_powermail.settings

        settings.setup {
            prefill {

                #                tx_nursing_stellenlink = TEXT
                #                tx_nursing_stellenlink.value = {field: positionlink}
            }

            main {
                form = {$plugin.tx_nursing.settings.applicationEmail.powermail.useFormPid}
                moresteps = 0
                confirmation = 0
            }

            thx {
                redirect = 12
                #body = Da
            }

            receiver {
                email = j.kolade@gmail.com
                subject = {$plugin.tx_nursing.settings.applicationEmail.subject}
                body = {powermail_all}
            }

            sender {
                name = {$plugin.tx_nursing.settings.applicationEmail.senderName}
                email = {$plugin.tx_nursing.settings.applicationEmail.senderEmail}
                subject = {$plugin.tx_nursing.settings.applicationEmail.subject}
                body = {powermail_all}
            }

            misc {
                addQueryString = 1

                htmlForLabels = 1
                htmlForHtmlFields = 1
            }
        }
    }
}
