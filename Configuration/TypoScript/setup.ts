config.contentObjectExceptionHandler = 0

plugin.tx_nursing {
	view {
		templateRootPaths.0 = EXT:nursing/Resources/Private/Templates/
		templateRootPaths.1 = {$plugin.tx_nursing.view.templateRootPath}
		partialRootPaths.0 = EXT:nursing/Resources/Private/Partials/
		partialRootPaths.1 = {$plugin.tx_nursing.view.partialRootPath}
		layoutRootPaths.0 = EXT:nursing/Resources/Private/Layouts/
		layoutRootPaths.1 = {$plugin.tx_nursing.view.layoutRootPath}

		widget {
			TYPO3\CMS\Fluid\ViewHelpers\Widget\PaginateViewHelper {
				templateRootPath = EXT:nursing/Resources/Private/Templates/
			}
		}
	}

	persistence {
		storagePid = {$plugin.tx_nursing.persistence.storagePid}
		recursive = 2
	}

	features {
		#skipDefaultArguments = 1
		# if set to 1, the enable fields are ignored in BE context
		ignoreAllEnableFieldsInBe = 0
		# Should be on by default, but can be disabled if all action in the plugin are uncached
		requireCHashArgumentForActionArguments = 1
	}

	mvc {
		#callDefaultActionIfActionCantBeResolved = 1
	}

	settings {
		devMode = 0

		listPID = {$plugin.tx_nursing.settings.listPID}
		employerListPID = {$plugin.tx_nursing.settings.employerListPID}
		ajaxpagetype = {$plugin.tx_nursing.settings.ajaxpagetype}
		employerAjaxpagetype = {$plugin.tx_nursing.settings.employerAjaxpagetype}
		formAjaxpagetype = {$plugin.tx_nursing.settings.formAjaxpagetype}

		coordfetchmaximum = {$plugin.tx_nursing.settings.coordfetchmaximum}

		list {
			showRegions = {$plugin.tx_nursing.settings.list.showRegions}
			showEmployer = {$plugin.tx_nursing.settings.list.showEmployer}
			pagination {
				itemsPerPage = 15
				insertAbove = 1
				insertBelow = 1
				maximumNumberOfLinks = 10
			}
		}

		legalsPageId = {$plugin.tx_nursing.settings.legalsPageId}

		applicationEmail {
			subject = {$plugin.tx_nursing.settings.applicationEmail.subject}

			bccName = {$plugin.tx_nursing.settings.applicationEmail.bccName}
			bccEmail = {$plugin.tx_nursing.settings.applicationEmail.bccEmail}

			senderName = {$plugin.tx_nursing.settings.applicationEmail.senderName}
			senderEmail = {$plugin.tx_nursing.settings.applicationEmail.senderEmail}

			overRideReceiver = {$plugin.tx_nursing.settings.applicationEmail.overRideReceiver}
			overRideReceiver {
				name = {$plugin.tx_nursing.settings.applicationEmail.overRideReceiver.name}
				email = {$plugin.tx_nursing.settings.applicationEmail.overRideReceiver.email}
				subjectPrefix = {$plugin.tx_nursing.settings.applicationEmail.overRideReceiver.subjectPrefix}
			}

            powermail.useFormPid = {$plugin.tx_nursing.settings.applicationEmail.powermail.useFormPid}
		}

		features {
			employerLinkInPositionList = {$plugin.tx_nursing.settings.features.employerLinkInPositionList}
			lightbox {
				enable = {$plugin.tx_nursing.settings.features.lightbox.enable}
				css-class = {$plugin.tx_nursing.settings.features.lightbox.css-class}
				rel-attribute = {$plugin.tx_nursing.settings.features.lightbox.rel-attribute}
			}
			useQuickApplicationForms = {$plugin.tx_nursing.settings.features.useQuickApplicationForms}
            usePowermailForQuickApplication = {$plugin.tx_nursing.settings.features.usePowermailForQuickApplication}
		}
	}
}

module.tx_nursing {
	view {
		templateRootPaths.0 = EXT:nursing/Resources/Private/Backend/Templates/
		#templateRootPaths.1 = {$plugin.tx_nursing.view.templateRootPath}
		partialRootPaths.0 = EXT:nursing/Resources/Private/Backend/Partials/
		#partialRootPaths.1 = {$plugin.tx_nursing.view.partialRootPath}
		layoutRootPaths.0 = EXT:nursing/Resources/Private/Backend/Layouts/
		#layoutRootPaths.1 = {$plugin.tx_nursing.view.layoutRootPath}
	}

	settings < plugin.tx_nursing.settings
	persistence < plugin.tx_nursing.persistence
	persistence {
		classes {
			Joekolade\Nursing\Domain\Model\Region.newRecordStoragePid = 6
			Joekolade\Nursing\Domain\Model\Location.newRecordStoragePid = 6
			Joekolade\Nursing\Domain\Model\Employer.newRecordStoragePid = 4
		}
	}
}

plugin.tx_nursing.persistence.storagePid = 3

// @var page PAGE

page.includeCSS {
	tx_nursing = EXT:nursing/Resources/Public/Css/nursing_styles.css
}

page.includeJSFooter {
	tx_nursing = EXT:nursing/Resources/Public/JavaScript/tx_nursing.js
}

# Bootstrap select

page.includeCSS {
	tx_nursing_bootstrap_select = EXT:nursing/Resources/Public/Css/select2/select2.css
}
page.includeJSFooter {
	tx_nursing_bootstrap_select = EXT:nursing/Resources/Public/JavaScript/select2/select2.min.js
}

# Parsley
page.includeCSS {
	tx_nursing_parsley = EXT:nursing/Resources/Public/bower_components/parsleyjs/src/parsley.css
}

page.includeJSFooter {
	tx_nursing_parsley = EXT:nursing/Resources/Public/bower_components/parsleyjs/dist/parsley.min.js
	tx_nursing_parsley_de = EXT:nursing/Resources/Public/bower_components/parsleyjs/dist/i18n/de.js
}

# Leaflet.js

page.includeCSS {
	leafletjs = https://unpkg.com/leaflet@1.2.0/dist/leaflet.css
	leafletjs.external = 1

	leafletjs_cluster = https://unpkg.com/leaflet.markercluster@1.2.0/dist/MarkerCluster.css
	leafletjs_cluster.external = 1

	leafletjs_cluster_default = https://unpkg.com/leaflet.markercluster@1.2.0/dist/MarkerCluster.Default.css
	leafletjs_cluster_default.external = 1
}

page.includeJSFooter {
	leafletjs = https://unpkg.com/leaflet@1.2.0/dist/leaflet.js
	leafletjs.external = 1
	leafletjs.external = 1

	leafletjs_cluster = https://unpkg.com/leaflet.markercluster@1.2.0/dist/leaflet.markercluster.js
	leafletjs_cluster.external = 1
}

lib.tx_nursing {
	math = TEXT
	math {
		current = 1
		prioriCalc = 1
	}
}

// Breadcrumb
//

[globalVar = GP:tx_nursing_list|position > 0]
	lib.nav.breadcrumb {
		# Disable active
		10 {
			1.CUR.wrapItemAndSub = <li class="breadcrumb-item">|</li>
			1.CUR.doNotLinkIt = 0
		}

		20 = CONTENT
		20 {
			table = tx_nursing_domain_model_position
			select {
				pidInList = {$plugin.tx_nursing.persistence.storagePid}
				uidInList = {GP:tx_nursing_list|position}
				uidInList.insertData = 1
				recursive = 9
			}

			renderObj = COA
			renderObj {
				wrap = <li class="breadcrumb-item active">|</li>

				10 = TEXT
				10.field = title

				20 = CONTENT
				20 {
					table = tx_nursing_domain_model_employer
					select {
						pidInList = {$plugin.tx_nursing.persistence.storagePid}
						uidInList.field = employer
						recursive = 9
					}

					renderObj = COA
					renderObj {
						stdWrap.noTrimWrap = | (|)|

						10 = TEXT
						10.field = title

						#                        15 = TEXT
						#                        15.noTrimWrap = |, ||
						#
						#                        20 = TEXT
						#                        20.field = geoaddress
					}
				}
			}
		}
	}
[global]

// Ajax page type
//

ajax_nursing = PAGE
ajax_nursing {
	typeNum = {$plugin.tx_nursing.settings.ajaxpagetype}
	config {
		disableAllHeaderCode = 1
		xhtml_cleaning = 0
		admPanel = 0
		additionalHeaders = Content-type: text/plain
		no_cache = 1
	}

	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = Nursing
		pluginName = List
		vendorName = Joekolade
		controller = Position
		switchableControllerActions {
			Position {
				1 = list
			}
		}

		view < plugin.tx_nursing.view
		persistence < plugin.tx_nursing.persistence
		settings < plugin.tx_nursing.settings
	}
}

ajax_nursing_employer < ajax_nursing
ajax_nursing_employer {
	typeNum = {$plugin.tx_nursing.settings.employerAjaxpagetype}

	10 {
		pluginName = Employerlist
		controller = Employer
		switchableControllerActions {
			Employer {
				1 = list
			}
		}

		view < plugin.tx_nursing.view
		persistence < plugin.tx_nursing.persistence
		settings < plugin.tx_nursing.settings
	}
}

ajax_nursing_quickform < ajax_nursing
ajax_nursing_quickform {
	typeNum = {$plugin.tx_nursing.settings.formAjaxpagetype}

	10 {
		switchableControllerActions {
			Position {
				1 = sendQuickApplication
			}
		}

		view < plugin.tx_nursing.view
		persistence < plugin.tx_nursing.persistence
		settings < plugin.tx_nursing.settings
	}
}

// Breadcrumbs
[globalVar = GP:tx_nursing|position > 0]
	lib.nav.breadcrumb {
		10.1.CUR.wrapItemAndSub = <li class="breadcrumb-item">|</li>
		10.1.CUR.doNotLinkIt = 0

		30 = CONTENT
		30.table = tx_nursing_domain_model_position
		30.select {
			#pidInList = {$plugin.tx_nursing.persistence.storagePid}
			pidInList = 7
			uidInList.data = GP:tx_nursing|position
		}

		30.renderObj = COA
		30.renderObj {
			wrap = <li class="breadcrumb-item active">|</li>
			10 = TEXT
			10 {
				field = title
			}

			#            30 = CONTENT
			#            30 {
			#                table = tx_nursing_domain_model_type
			#                select {
			#                    pidInList = 5
			#                    uidInList.field = types
			#                }
			#                renderObj = TEXT
			#                renderObj {
			#                    noTrimWrap = | / ||
			#                    field = title
			#                }
			#            }
		}
	}
[global]

// Cs seo sitemap
// see: https://docs.typo3.org/typo3cms/extensions/cs_seo/Developer/ExtendSitemap/Index.html
//

plugin.tx_csseo.sitemap {
	extensions {
		jobs {
			table = tx_nursing_domain_model_position
			additionalParams = tx_nursing_list[controller]=Position&tx_nursing_list[action]=show&tx_nursing_list[position]
			useCacheHash = 1
			additionalWhereClause =
			storagePid = 3
			recursive = 5
			detailPid = 33
		}

		# employers {
		# Not needed since the detail pages are usual TYPO3 pages
		# }
	}
}

[globalVar = TSFE : beUserLogin > 0]
	plugin.tx_nursing.settings.devMode = 1
[global]

// Features
//
[globalVar = LIT:{$plugin.tx_nursing.settings.features.usePowermailForQuickApplication} > 0]
    <INCLUDE_TYPOSCRIPT: source="FILE:EXT:nursing/Configuration/TypoScript/Features/PowermailQuickapplication.ts">
[global]
