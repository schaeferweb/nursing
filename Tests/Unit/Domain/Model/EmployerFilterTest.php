<?php
namespace Joekolade\Nursing\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Joe Schäfer <mail@schaefer-webentwicklung.de>
 */
class EmployerFilterTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Joekolade\Nursing\Domain\Model\EmployerFilter
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Joekolade\Nursing\Domain\Model\EmployerFilter();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getLocsReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLocs()
        );
    }

    /**
     * @test
     */
    public function setLocsForStringSetsLocs()
    {
        $this->subject->setLocs('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'locs',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLocationsReturnsInitialValueForLocation()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getLocations()
        );
    }

    /**
     * @test
     */
    public function setLocationsForObjectStorageContainingLocationSetsLocations()
    {
        $location = new \Joekolade\Nursing\Domain\Model\Location();
        $objectStorageHoldingExactlyOneLocations = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneLocations->attach($location);
        $this->subject->setLocations($objectStorageHoldingExactlyOneLocations);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneLocations,
            'locations',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addLocationToObjectStorageHoldingLocations()
    {
        $location = new \Joekolade\Nursing\Domain\Model\Location();
        $locationsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $locationsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($location));
        $this->inject($this->subject, 'locations', $locationsObjectStorageMock);

        $this->subject->addLocation($location);
    }

    /**
     * @test
     */
    public function removeLocationFromObjectStorageHoldingLocations()
    {
        $location = new \Joekolade\Nursing\Domain\Model\Location();
        $locationsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $locationsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($location));
        $this->inject($this->subject, 'locations', $locationsObjectStorageMock);

        $this->subject->removeLocation($location);
    }

    /**
     * @test
     */
    public function getRegionsReturnsInitialValueForRegion()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getRegions()
        );
    }

    /**
     * @test
     */
    public function setRegionsForObjectStorageContainingRegionSetsRegions()
    {
        $region = new \Joekolade\Nursing\Domain\Model\Region();
        $objectStorageHoldingExactlyOneRegions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneRegions->attach($region);
        $this->subject->setRegions($objectStorageHoldingExactlyOneRegions);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneRegions,
            'regions',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addRegionToObjectStorageHoldingRegions()
    {
        $region = new \Joekolade\Nursing\Domain\Model\Region();
        $regionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $regionsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($region));
        $this->inject($this->subject, 'regions', $regionsObjectStorageMock);

        $this->subject->addRegion($region);
    }

    /**
     * @test
     */
    public function removeRegionFromObjectStorageHoldingRegions()
    {
        $region = new \Joekolade\Nursing\Domain\Model\Region();
        $regionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $regionsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($region));
        $this->inject($this->subject, 'regions', $regionsObjectStorageMock);

        $this->subject->removeRegion($region);
    }
}
