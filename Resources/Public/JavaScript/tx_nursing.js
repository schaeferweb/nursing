// Namespace
var
	Joekolade = Joekolade || {},

	xhr = new window.XMLHttpRequest()
;

Joekolade.map = {
	container: $('.tx-nursing'),
	mapid: 'tx-nursing__map',
	positionsSelector: '.tx-nursing__item',
    map: null,
    maxZoom: false,
	cluster: '',
    flyTimeout: 0,
	clusterOptions: {
		showCoverageOnHover: false
	},
    mapOptions: {
        // Germany
        center: ['51.133', '10.018'],
        zoom: 6,
        scrollWheelZoom: this.scrollWheelZoom
    },
	scrollWheelZoom: false,
	mapboxAccessToken: 'pk.eyJ1Ijoiam9la29sYWRlIiwiYSI6ImNqYTgyZDgzejAyZDEycXFxcDQ5emFibHUifQ.h7qrFGifAqvJnKAKhbgz8g',
    pflegeberufStyleUrl: 'mapbox://styles/joekolade/cjkxs5wgn2x862ruj25534obh',

	// Init Map
	initialize: function () {
        this.map = L.map(this.mapid, this.mapOptions);

		this.cluster = L.markerClusterGroup(this.clusterOptions);

		this.addMapLayer();

		this.resetMarkers(this.map);

		return this.map;
	},

	addMapLayer: function(){
		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
			//attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
			maxZoom: 20,
			id: 'mapbox.streets',
			accessToken: this.mapboxAccessToken
		}).addTo(this.map);
	},

	// Actualize markers
	resetMarkers: function(map){

		var
			$items = $(this.positionsSelector),
			_self = this
		;

        console.log($items);

		mapMarkers = [];

		_self.clearMarkers(map);

		_self.cluster = L.markerClusterGroup(_self.clusterOptions);

		// if (staticMapMarkers != null) {
		// 	for (var i = 0; i < staticMapMarkers.length; i++) {
		// 		if (staticMapMarkers[i].length) {
		// 			marker = L.marker(staticMapMarkers[i]['geo']);
		// 			_self.cluster.addLayer(marker);
		// 			mapMarkers.push(marker);
		// 		}
		// 	}
		// }
		// else {
			$items.each(function () {
				var
					$i = $(this),
					splitter = /\s*,\s*/,
					geo = $i.data('geo') ? $i.data('geo').split(splitter) : ''
				;

				// markerClustering
				// https://asmaloney.com/2015/06/code/clustering-markers-on-leaflet-maps/
				if (geo.length) {
					// Add marker to map
					marker = L.marker(geo)
						.bindPopup(function () {
							var $c = $i.clone();
							// Header
							$('div[itemprop="description"]', $c).remove();
							return $c.html();
						});
					_self.cluster.addLayer(marker);
					mapMarkers.push(marker);
				}
			});
		// }

		_self.cluster.addTo(map);

		if(mapMarkers.length > 0){
            setTimeout(function () {
                _self.centerLeafletMapOnMarker(map, mapMarkers);
            }, _self.flyTimeout);
		}

	},

	// Clear all markers on map
	clearMarkers: function(map){
		var _self = this;
		for (var i = 0; i < mapMarkers.length; i++) {
			_self.cluster.removeLayer(mapMarkers[i]);
		}
		map.removeLayer(_self.cluster);
	},

	// reninit after loading of new positions
	reinitMap: function(map){

		this.clearMarkers(map);

		this.resetMarkers(map);
	},

	// recenter Map
	centerLeafletMapOnMarker: function(map, marker) {
		var group = new L.featureGroup(marker);
        var flyOptions = {
            paddingTopLeft: [10, 5],
            paddingBottomRight: [10, 5]
        };
        if (this.maxZoom) {
            flyOptions.maxZoom = this.maxZoom;
        }
        //map.fitBounds(group.getBounds());
        map.flyToBounds(this.cluster, flyOptions);
	}
};

Joekolade.minimap = Joekolade.map;
Joekolade.minimap.mapid = 'tx-nursing__jobmap';
Joekolade.minimap.scrollWheelZoom = false;
Joekolade.minimap.positionsSelector = '.tx-nursing-detail';
Joekolade.minimap.maxZoom = 8;
Joekolade.minimap.flyTimeout = 1200;

Joekolade.applicationForm = {

	$form: $(),

	applicationFormSelector: '.quickApplicationForm',

	parsleyOptions: {},


	init: function () {
		$(this.applicationFormSelector).parsley(this.parsleyOptions);

		this.$form = $(this.applicationFormSelector);

		setTimeout(function () {
			$('[type="submit"][disabled]').removeAttr('disabled');
		}, 1000);

		// Blend in Form - and hide again if screen is small
		this.$form.addClass('show');

		// Prevent static submit
		this.$form.on('submit', function (e) {

			e.preventDefault();
			var
				$applicationForm = $(this),
				ajaxData = $applicationForm.serialize()
			;

			xhr.abort();
			$applicationForm.css({
				opacity: 0.5
			});

			showOverlay();

			$.ajax({
				data: ajaxData,
				method: 'GET',
				url: $applicationForm.attr('action'),
				xhr: function () {
					return xhr;
				},
				success: function (data) {
					var $target = $(data).find('#quickApplicationForm');

					console.log($target);
					if ($target.length) {
						$applicationForm.replaceWith($target);
						$target.addClass('show');
					}
					else {
						$applicationForm.replaceWith(data);
					}
				}
			});

			return false;
		});

		return this.$form;
	}
};

$(function () {
	var
		$container = $('.tx-nursing'),
		$aside = $('.tx-nursing__aside', $container),
		$content = $('.tx-nursing__content', $container),
		// holder for ajax event

		mapId = 'tx-nursing__map',
		globalMap = null,
        miniMap = null,
		mapMarkers = [],

		isEmployerList = $('.tx-nursing-list', $container).hasClass('prefilter');
    isShowjob = $('.tx-nursing-detail', $container).length || false;

		// Overlay handling
		showOverlay = function () {
			$container.addClass('show-overlay');
			$('.tx-nursing-list.prefilter', $container).removeClass('prefilter');
		},
		hideOverlay = function () {
			$container.removeClass('show-overlay');
		},

		_initFilter = function () {

			var
				$form = $('form', $container),
				$locationWrapper = $('.locationWrapper', $form),
				$locations = $('#filterLocations', $form),
				$regions = $('#filterRegions', $form),
				$locs = $('#filterLocs', $form),
				// Optgroups to fill
				$locsLocations = $('optgroup[property="cities"]', $locs),
				$locsRegions = $('optgroup[property="regions"]', $locs),

				fillUpCombinedSelect = function () {

					var filledUp = false;

					$('option', $regions).each(function () {
						var $c = $(this).clone();
						$c.attr('value', 'r-' + $c.attr('value'));
						$c.appendTo($locsRegions);
						filledUp = true;
					});
					$('option', $locations).each(function () {
						var $c = $(this).clone();
						$c.attr('value', 'l-' + $c.attr('value'));
						$c.appendTo($locsLocations);
						filledUp = true;
					});

					if (filledUp) {
						$locationWrapper.addClass('locs-replaces');
					}
				}
			;

			// build locs
			fillUpCombinedSelect();

			// events
			$('#filterLocs').on('select2:select  select2:unselect', function (e) {
				var
					locVal = [],
					regVal = [],
					v = $(e.target).val()
				;

				$.each(v, function (idx, val) {
					if (val.substring(0, 2) == 'l-') {
						locVal.push(val.substring(2));
					}
					else if (val.substring(0, 2) == 'r-') {
						regVal.push(val.substring(2));
					}
				});
				if (locVal.length) {
					$locations.val(locVal);
				}
				else {
					$locations.val('');
				}
				if (regVal.length) {
					$regions.val(regVal);
				}
				else {
					$regions.val('');
				}
			});

			// Blend in Form - and hide again if screen is small
			$form.addClass('show');

			$('.selectpicker2').select2({
				language: {
					errorLoading: function () {
						return "Die Ergebnisse konnten nicht geladen werden."
					}, inputTooLong: function (e) {
						var t = e.input.length - e.maximum;
						return "Bitte " + t + " Zeichen weniger eingeben"
					}, inputTooShort: function (e) {
						var t = e.minimum - e.input.length;
						return "Bitte " + t + " Zeichen mehr eingeben"
					}, loadingMore: function () {
						return "Lade mehr Ergebnisse…"
					}, maximumSelected: function (e) {
						var t = "Sie können nur " + e.maximum + " Eintr";
						return e.maximum === 1 ? t += "ag" : t += "äge", t += " auswählen", t
					}, noResults: function () {
						return "Keine Übereinstimmungen gefunden"
					}, searching: function () {
						return "Suche…"
					}
				}
			});

			// Toggle form
			$(window).on('resize', function () {

				if ( ($(window).width() < 992) && !($('.tx-nursing-list.prefilter').length) ) {
					setTimeout(function () {
						$('.tx_nursing-form.collapse', $aside).removeClass('show');
					}, 20);
				}
				else {
					$('.tx_nursing-form.collapse', $aside).addClass('show');
				}
			});

			$(window).resize();

			_initAjax();
		},

		_initAjax = function () {
			// Filter via ajax
			//

			var
				$form = $('form', $aside),

				ajaxtype = $form.data('ajaxtype')
			;

			$form.addClass('ajax-form');
			$form.prepend(function () {
				return $('<input />').attr({
					value: ajaxtype,
					type: 'hidden',
					name: 'type',
					id: 'type',
				});
			});

			$.ajaxSetup({
				url: $form.attr('action'),
				type: 'GET',
				contentType: "application/html; charset=utf-8",
				datatype: 'html',
				cache: false,
				timeout: 5000,

				error: function() {
				},

				success: function (data) {
					$('.tx-nursing__content').replaceWith(data);

					if($(data).hasClass('prefilter')){
						$('.tx-nursing-list', $container).addClass('prefilter');

						$('.selectpicker2').select2('close');
						setTimeout(function () {
							$('.selectpicker2').select2('open').val(val);
						}, 310);
					}
				},

				complete: function () {
					hideOverlay();
					if (isEmployerList) {
						globalMap.closePopup();
						Joekolade.map.resetMarkers(globalMap);
					}
				}
			});

			$aside.on('select2:select select2:unselect', '.selectpicker2', function (e) {
				e.preventDefault();

				var ajaxData = $form.serialize();

				xhr.abort();

				showOverlay();

				if ($form.data('usenoajax')) {
					$('input[name="type"]', $form).val('');
					return $form.submit();
				}

				$.ajax({
					data: ajaxData,
					xhr : function(){
						return xhr;
					}
				});
			});

			// paginator
			$container.on('click', 'nav[aria-label="page navigation"] a.page-link', function (e) {

				e.preventDefault();

				var ajaxtype = $('input[name="type"]', $form).val();

				xhr.abort();

				showOverlay();

				if ($form.data('usenoajax')) {
					$('input[name="type"]', $form).val('');
					return $form.submit();
				}

				$.ajax({
					//data: ajaxData,
					url: $(this).attr('href') + '&type=' + ajaxtype,
					xhr : function(){
						return xhr;
					}
				});
			});

		},

		_initMap = function() {
			if (isEmployerList) {
				// Add map container
				$('<div id="' + mapId + '" />').appendTo($('.stage'));
				$('#' + mapId).addClass('open');
				globalMap = Joekolade.map.initialize();
			}
		},

            _initMiniMap = function () {
                if (isShowjob) {
                    miniMap = Joekolade.minimap.initialize();
                }
            },

		init = function () {

			_initFilter();

			_initMap();

            _initMiniMap();
		}
	;

	init();

	var $applicationForm = Joekolade.applicationForm.init();
});


